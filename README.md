# Hello this is small **guide** in this template
### 1: Run - `npm i` to install packages that needed to start this template
### 2: Run ESlint to find and fix problems in your JavaScript code using - `npm run lint` (if you edited code to your purpose)
### 3: Run `npm run test` - to run jest tests
### 4: Run `npm start` - to run this template
### 5: Run `npm run build:css` - to build(concat, minify) output css file
For other available commands - check **package.json** file